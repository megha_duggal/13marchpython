def person(**kwargs):
    #return kwargs
    # for a in kwargs:
    #     return ' {} = {}'.format(a,kwargs[a])
        #print('abcd')        this will not print bcz return statement is the final statement of function and after return statement no other statement will execute
    for x,y in kwargs.items():
        print('{}={}'.format(x,y))
        print('\n')
a=person(name='Abcd',age=25) 
b=person(name='python',age=30,hobby='reading')
c=person(name='python',age=30,hobby='reading',roll_no='1')
print(a)
print(b)
print(c)   